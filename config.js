'use strict';

module.exports = {
  url: 'https://mnepita.me',
  siteUrl: 'https://mnepita.me',
  pathPrefix: '/',
  title: 'Personal blog by Martín Nepita',
  subtitle: 'Full Stack Web Developer',
  copyright: 'made with lotta 💜',
  disqusShortname: '',
  postsPerPage: 7,
  googleAnalyticsId: 'UA-154424551-1',
  useKatex: false,
  menu: [
    {
      label: 'Articles',
      path: '/'
    },
    {
      label: 'About',
      path: '/pages/about'
    },
    {
      label: 'Contact',
      path: '/pages/contact'
    }
  ],
  author: {
    name: 'Martín Nepita',
    photo: '/media/profile.jpg',
    bio: 'Full Stack Web Developer💻 & amateur photographer 📸.',
    contacts: {
      email: '',
      facebook: '',
      telegram: '',
      twitter: '_mnepita',
      github: 'mnepita',
      rss: '',
      vkontakte: '',
      linkedin: 'martin-nepita/',
      instagram: '_mnepita/',
      line: '',
      gitlab: 'mnepita',
      weibo: '',
      codepen: 'mnepita'
    }
  }
};


plugins: [`gatsby-plugin-react-helmet`]

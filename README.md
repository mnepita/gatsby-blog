## Source for personal blog hosted on Netlify.
The main purpose of this blog is to serve as my main Online Notebook to be able to keep track of my developer journey. I started this blog on December 2019.

**Technologies used:**

- [Gatsby.JS](https://www.gatsbyjs.org)
- [Netlify](https://www.netlify.com)
- [Lumen Starter](https://www.gatsbyjs.org/starters/alxshelepenok/gatsby-starter-lumen)

**Live Demo**
[Martin Nepita | mnepita.me](https://mnepita.me)

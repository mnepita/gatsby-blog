---
title: "About me"
template: "page"
draft: false
socialImage: "/media/martin.jpg"
---
![Martín Nepita](/media/martin.jpg "Martín Nepita")

I am **Martín Nepita** a self taught web developer from Mexicali, Mexico who is in love with code,  building exciting and useful applications that can help in the solving of day to day tedious problems. I have a big passion for music & [photography](https://www.flickr.com/people/martinnepita/) 📸. 


## Skills

- HTML5 | CSS | JavaScript
- Boostrap | Materialize
- React.js
- Express.js
- Node.js
- Gatsby
- MongoDB
- Github

## Software Experience

- Photoshop |  Adobe XD | Lightroom
- Windows, MacOS, Linux (Ubuntu)



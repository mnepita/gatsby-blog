---
template: post
title: Data Types in JavaScript
slug: Data-Types
draft: false
date: 2019-12-11T03:53:00.000Z
description: 'Data types explained.'
category: DaysOfCode
tags:
  - JavaScript
  - DataTypes
---
![Macbook](/media/macbook.jpg)

# What are Data Types?

Data type in programming is an attribute of data which tells the compiler or interpreter how the programmar intends to use the data. There are a few different types of data we can store in variables.

In JavaScript there are 6 kinds of primitive types and objects. Data types can be categorized into two types called `Primitive type` and `Object` (Composite type).

## Primitive Data Types

## `Number`

Numbers can be written with or without a decimal point. A number can also be `Infinity`, `-Infinity`, and `NaN` (not a number).  Many mathematical operations are carried out using these numbers.

```javascript
var number1 = 25;
var float = 3.1416;

var total = number1 + float;

console.log(total);  // Returns 28.1416
```

There are many operations for numbers, e.g. multiplication `*` , division  `/` , addition `+` , subtraction  `-` , and so on.

## `String`

Strings are used for storing data that involves characters like text, names, addresses,etc.. Strings must be inside of either double or single quotes. In JavaScript, Strings are immutable (they cannot be changed); other wise JavaScript tries to interpret it as another variable name. You can perform operations like string concatenation (combining 2 strings of characters together).

```javascript
var firstName = "Quincy";
var lastName = "Larson";

var fullName = firstName + " " +  lastName;//Empy string is used to add an space in between the strings.

console.log(fullName);//Returns Quincy Larson
```

>  **As a best practice programmers use [camelCase🐫](https://en.wikipedia.org/wiki/Camel_case) for better readability when naming their variables**
>
> ```javascript
> var thisIsMyFirstName = “Martín”; //uses camelCase
> console.log(firstName); // outputs “Martín”
>
> var thisismyfirstvariable = "First variable"; // does not uses camelCase & it is really hard to read.
> ```

## `Boolean`

A boolean represents only one of two values: `true` , or  `false`. Think of a boolean as an on/off or a yes/no switch. It is usually used to check if somethings is correct or incorrect (comparison).

```javascript
var isGreater = 4 > 1;

alert( isGreater ); // true (the comparison result is "yes")
```

## `Null`

Is just a special value which represents "nothing": null. It is explicitly nothing.

```javascript
var nothing = null;
```

## `Undefined`

Similar to `Null` a variable that has no value is undefined.

```javascript
var testVar;
```

```javascript
console.log(testVar); // undefined
```

## Non-primitive data types: 

Data types which are created by the programmer and not pre-defined by the language itself.

## `Object`

The `object` (Composite Data Type) type is special. An object is created by the user for a defined purpose. The properties of an object define its characteristics and can be accessed with the 'dot-notation'.

```javascript
var person = new Object();
  person.name = 'Eon Musk';   //properties
  person.age = 21;
  person.profession = 'Developer';

console.log("My name is " + person.name  + " and I work as a " + person.profession + " at Google");

//Created Object
person {
name: "Eon Musk",
age: 21,
profession: "Developer"
}
```

## `Arrays`

In JavaScript, an `array` is a single variable that is used to store different elements. This is an **ordered collection** of values. 

Each element has a numeric position in the array known as its **index**. These elements are numbered starting with zero & we can get an element by its number in square brackets:


```javascript
var fruits = ["Apple", "Orange", "Banana"];

console.log(fruits[0]); // Apple
console.log(fruits[1]); // Orange
console.log(fruits[2]); // Banana
```

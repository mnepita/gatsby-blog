---
template: post
title: 'Learning Arrays and Objects'
slug: /posts/arrays-and-objects
draft: false
date: 2019-12-18T03:53:00.000Z
description: >-
  Learning Arrays & Objects... How to manipulate them!
category: 'DaysOfCode'
tags:
  - Web Development
---

## What is an Object?

The `object` (Composite Data Type) type is special. An object is created by the user for a defined purpose. The properties of an object define its characteristics and can be accessed with the 'dot-notation'. 

An Object is similar to a real object in life you know; for example: a person, a house, a dog, etc.

To create an object with properties, you use the **name:value** syntax. For example the following code creates a person object:

```javascript
var person = {
//properties are defined
  'name': 'Jeff Bezos',
  'age': 21,
  'profession': 'Developer',
  'workplace': 'Amazon'
}

console.log("My name is " + person.name  + " , I work as " + person.profession + " at " + person.workplace);
```



## What is an Array?

In JavaScript, an `array` is a single variable that is used to store different elements. This is an **ordered collection** of values. 

Each element has a numeric position in the array known as its **index**. These elements are numbered starting with zero & we can get an element by its number in square brackets:


```javascript
var fruits = ["Apple", "Orange", "Banana"];

console.log(fruits[0]); // Apple
console.log(fruits[1]); // Orange
console.log(fruits[2]); // Banana
```

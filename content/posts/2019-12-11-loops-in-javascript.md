---
template: post
title: Loops in JavaScript
slug: js-Loops
draft: false
date: 2019-12-12T06:23:08.211Z
description: 'For, While and Do While Loop in JavaScript.'
category: DaysOfCode
tags:
  - JavaScript
  - Loops
---
![Roller Coaster](/media/germanyRollercoaster.jpg)
## Why Loops?

Loops allows you to run the code repeatedly, for a specific number of times or as long as a specific conditions is true. For example, suppose we want to print “Hello World” 10 times. This can be done in two ways as shown below:

**Iterative Method**

```javascript
console.log("Hello, World!);
console.log("Hello, World!);
console.log("Hello, World!);
console.log("Hello, World!);
console.log("Hello, World!);
console.log("Hello, World!);
console.log("Hello, World!);
console.log("Hello, World!);
console.log("Hello, World!);
console.log("Hello, World!);
```

**Using Loops**

In Loop, the statement needs to be written only once and the loop will be executed 10 times as shown below:

```javascript
var i; 
  
for (i = 0; i < 10; i++)  
{ 
    console.log("Hello World!"); 
} 
```

In computer programming, a loop is a sequence of instructions that is repeated until a certain condition is reached.

## `for` statement

A [`for`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/statements/for) loop repeats until a specified condition evaluates to `false`.

**Syntax**

```javascript
for ([initialExpression]; [condition]; [incrementExpression])
  statement
```

When a `for` loop executes, the following occurs:

1. The initializing expression `initialExpression`, if any, is executed. This expression usually initializes one or more loop counters, but the syntax allows an expression of any degree of complexity. This expression can also declare variables.
2. The `condition` expression is evaluated. If the value of `condition` is true, the loop statements execute. If the value of `condition` is false, the `for` loop terminates. (If the `condition` expression is omitted entirely, the condition is assumed to be true.)
3. The `statement` executes. To execute multiple statements, use a block statement (`{ ... }`) to group those statements.
4. If present, the update expression `incrementExpression` is executed.
5. Control returns to Step 2.

```javascript
for(let i = 0 ;i < 5; i++) {
    console.log(i);
}
```

## `do...while` statement

The [`do...while`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/statements/do...while) statement repeats until a specified condition evaluates to false.

```javascript
do
  statement
while (condition);
```

_`statement`_ is always executed once before the condition is checked. (To execute multiple statements, use a block statement (`{ ... }`) to group those statements).

If `condition` is `true`, the statement executes again. At the end of every execution, the condition is checked. When the condition is `false`, execution stops, and control passes to the statement following `do...while`.

```js
let i = 0;
do {
  i += 1;
  console.log(i);
} while (i < 5);
```

## `while` statement

A [`while`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/statements/while) statement executes its statements as long as a specified condition evaluates to `true`. A `while` statement looks as follows:

```
while (condition)
  statement
```

If the _`condition`_becomes `false`, `statement` within the loop stops executing and control passes to the statement following the loop.

The condition test occurs _before_ `statement` in the loop is executed. If the condition returns `true`, `statement` is executed and the _`condition`_ is tested again. If the condition returns `false`, execution stops, and control is passed to the statement following `while`.

To execute multiple statements, use a block statement (`{ ... }`) to group those statements.

```js
let n = 0;
let x = 0;
while (n < 3) {
  n++;
  x += n;
}
```

With each iteration, the loop increments `n` and adds that value to `x`. Therefore, `x` and `n` take on the following values:

* After the first pass: `n` = `1` and `x` = `1`
* After the second pass: `n` = `2` and `x` = `3`
* After the third pass: `n` = `3` and `x` = `6`

After completing the third pass, the condition `n < 3` is no longer `true`, so the loop terminates.

---
template: post
title: 'Hello, World!'
slug: /posts/hello-world/
draft: false
date: 2019-12-09T05:40:00.000Z
description: >-
  I’ve officially started my Web Development journey just a month ago by
  subscribing to “The complete 2019 Web Development Bootcamp” from the London
  App Brewery
category: 'Days Of Code'
tags:
  - Web Development
---
![Hello World](/media/helloWorld.jpg)

I’ve officially started my Web Development journey just a month ago by subscribing to “The complete 2019 Web Development Bootcamp” from the [London App Brewery](https://www.londonappbrewery.com/).

This is an amazing course teach by [Angela Yu](https://twitter.com/yu_angela), she has a talented way to explain to you the topics on the course and it is one highest rated on Udemy. From the building foundations of the web (HTML, CSS, JS) to the most modern frameworks such as NodeJS, ExpressJS, React, MongoDB, and many more with over 50 hours.

As I continue to learn new technologies & grow as a future Web Developer, the only way to really learn new skills is by putting them into practice and building projects, this is one of them.

This blog has been created with the idea of having a personal online notebook that I can access at anytime from anywhere in the world, to help me and others understand the weird parts of this exciting journey. 💻


> “The only source of knowledge is the experience.”
>
> Albert Einstein
